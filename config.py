INPUT_IMAGE = 'resource/rainbow.jpg'
NOISE_TYPE = 'gaussian' # 'gaussian' or 'salt and pepper'
NOISE_SIGMA = 0.65

GA_POPULATION_SIZE = 6
GA_ITERATION_MAX = 100
GA_MUTATION_RATE = 0.1
GA_FITNESS_LAMBDA = 0.075

BATCH_MODE = False
