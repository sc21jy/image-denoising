# coding: utf-8

import numpy as np

def SNR(image, corrupted_image):
    noise = corrupted_image - image

    image_power = np.average(image ** 2)
    noise_power = np.average(noise ** 2)
    return 10 * np.log10(np.abs(image_power-noise_power)/noise_power)

def PSNR(image1, image2):
    MSE = np.mean((np.array(image1) - np.array(image2)) ** 2)
    return 20 * np.log10(255 / (MSE ** 0.5))

def SSIM(image1, image2):
    c1 = (0.01 * 255) ** 2
    c2 = (0.03 * 255) ** 2
    mean1 = np.mean(image1)
    mean2 = np.mean(image2)

    m, n, _ = image1.shape
    var1 = np.sqrt(np.sum((image1 - mean1) ** 2) / (m * n - 1))
    var2 = np.sqrt(np.sum((image2 - mean2) ** 2) / (m * n - 1))

    # covariance
    cov = np.sum((image1 - mean1) * (image2 - mean2)) / (m * n - 1)
    return (2 * mean1 * mean2 + c1) * (2 * cov + c2) / ((mean1 ** 2 + mean2 ** 2 + c1) * (var1 ** 2 + var2 ** 2 + c2))
