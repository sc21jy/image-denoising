import imp
import config
import imagefunctions
from filter import gaussian_blur, BM3D, average_filter, median_filter, non_local_mean
import ga
import metrics
import log
import cv2 as cv
import numpy as np

if __name__ == '__main__':
    for iter_times in range(9):
        for i in range(5, 105, 5):
            imagefunctions.generate_noise_image(config.INPUT_IMAGE, i/100)

        sLogFile = ''
        if config.NOISE_TYPE == 'gaussian':
            sLogFile = 'filter_under_gaussian_noise_{}'.format(iter_times+1)
        else:
            sLogFile = 'filter_under_salt_and_pepper_noise_{}'.format(iter_times+1)

        for i in range(5, 105, 5):
            sigma = i / 100

            image = None
            if config.NOISE_TYPE == 'gaussian':
                image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), 1)
            else:
                image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), 1)

            clean_image = cv.imread('tmp/image.tiff', 1)

            log.log(sLogFile, 'noise std:{}'.format(sigma))
            print('noise SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))
            log.log(sLogFile, 'noise image SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))

            for function in (BM3D, gaussian_blur, median_filter, average_filter, non_local_mean):
                denoised_image = function(image)
                print('{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))
                log.log(sLogFile, '{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))

        sGALogFile = ''
        if config.NOISE_TYPE == 'gaussian':
            sGALogFile = 'ga_under_gaussian_noise_{}'.format(iter_times+1)
        else:
            sGALogFile = 'ga_under_salt_and_pepper_noise_{}'.format(iter_times+1)

        for i in range(5, 105, 5):
            sigma = i / 100

            noise_image = None
            if config.NOISE_TYPE == 'gaussian':
                noise_image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), 1)
            else:
                noise_image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), 1)
            
            np.random.seed(0)
            obj = ga.GA(noise_image)
            obj.run(debug_mode=sigma, sLogFile=sGALogFile)

