from PIL import Image

for sImageDir in ('resource/cloud.jpg', 'resource/firework.jpg', 'resource/flamingo.jpg', 'resource/food.jpg', 'resource/leaves.jpg', 'resource/moon.jpg', 'resource/panda.jpg', 'resource/rainbow.jpg'):
    count = 0
    luminance = 0
    im = Image.open(sImageDir)

    for w in range(im.width):
        for h in range(im.height):
            count += 1
            r, g, b = im.getpixel((w, h))
            luminance += (0.2126*r + 0.7152*g + 0.0722*b)

    print(sImageDir, luminance/count)

    

