import datetime

def log(filename, *lstStr):
    sFile = 'log/' + filename + '.txt'
    fi = open(sFile, "a+")
    fi.write('[{}]'.format(datetime.datetime.now().strftime('%m-%d %H:%M:%S')) + ' '.join(lstStr) + "\n")
    fi.close()
