import numpy as np
import config
import filter
import log
import cv2 as cv
import metrics
import tensorflow as tf

class GA(object):
    def __init__(self, image):
        self.image = image
        self.image_shape = image.shape

    def init_population(self):
        avg_denoised = filter.average_filter(self.image)
        print('population initial complete 1/{}'.format(config.GA_POPULATION_SIZE))
        median_denoised = filter.median_filter(self.image)
        print('population initial complete 2/{}'.format(config.GA_POPULATION_SIZE))
        gassian_blur_denoised = filter.gaussian_blur(self.image)
        print('population initial complete 3/{}'.format(config.GA_POPULATION_SIZE))
        nonlocalmean_denoised = filter.non_local_mean(self.image)
        print('population initial complete 4/{}'.format(config.GA_POPULATION_SIZE))

        population = np.zeros((config.GA_POPULATION_SIZE, *self.image_shape), dtype=np.uint8)
        parents = np.stack((avg_denoised, median_denoised, gassian_blur_denoised, nonlocalmean_denoised))
        population[0:4] = parents.copy()
        m, n, c = self.image_shape
        for child_index in range(4, config.GA_POPULATION_SIZE):
            indexs = np.random.choice(4, 2, replace=False)   # choose 2 parents from 4
            pairs = parents[indexs]
            new_individual = np.zeros((self.image_shape))
            for i in range(m):
                for j in range(n):
                    for k in range(c):
                        new_individual[i, j, k] = pairs[np.random.randint(2)][i, j, k]
            population[child_index] = new_individual.astype('uint8')
            print('population initial complete {}/{}'.format(child_index+1, config.GA_POPULATION_SIZE))
        return population

    def fitness(self, image):
        fidelity = config.GA_FITNESS_LAMBDA * np.sum(np.abs(image - self.image))
        total_variation = tf.image.total_variation(tf.convert_to_tensor(tf.cast(image, tf.float32)))
        return fidelity + total_variation

    def fitness_list(self, images):
        fitness = []
        for image in images:
            fitness.append((self.fitness(image), image))
        return fitness

    def selection(self, population, fitness_val_list):
        fit_value = -np.array(fitness_val_list)
        total_fit = np.sum(fit_value)
        
        probability = fit_value / total_fit
        probability = np.cumsum(probability)
        parent_probability = np.sort(np.random.random(2))
        
        index = 0
        parent_count = 0
        parent = np.zeros((2, *self.image_shape), dtype=np.uint8)
        while parent_count < 2:     # roulette wheel selection
            if probability[index] > parent_probability[parent_count]:
                parent[parent_count] = population[index]
                parent_count += 1
            else:
                index += 1
        return parent

    def crossover(self, parent):
        m, n, _ = self.image_shape
        crossover_type = np.random.randint(6)
        if crossover_type == 0:     # One point column
            col = np.random.randint(n)
            new_individual = parent[0]
            new_individual[:, col:] = parent[1][:, col:]
        elif crossover_type == 1:   # One point row
            row = np.random.randint(m)
            new_individual = parent[0]
            new_individual[row:, :] = parent[1][row:, :]
        elif crossover_type == 2:   # Two point column
            cols = np.sort(np.random.randint(0, n, 2))
            new_individual = parent[0]
            new_individual[:, cols] = parent[1][:, cols]
        elif crossover_type == 3:   # Two point row
            rows = np.sort(np.random.randint(0, m, 2))
            new_individual = parent[0]
            new_individual[rows, :] = parent[1][rows, :]
        elif crossover_type == 4:   # Cross grid
            row = np.random.randint(m)
            col = np.random.randint(n)
            new_individual = parent[0]
            new_individual[0:row, 0:col] = parent[1][0:row, 0:col]
            new_individual[row:, col:] = parent[1][row:, col:]
        else:   # Pixel-by-pixel random
            new_individual = parent[0]
            for i in range(m):
                for j in range(n):
                    if np.random.random() <= 0.5:
                        new_individual[i, j] = parent[1][i, j]
        return new_individual.astype('uint8')

    def mutation(self, image):
        if np.random.random(1) > config.GA_MUTATION_RATE:
            return image

        mutation_type = np.random.randint(4)
        if mutation_type == 0:
            return filter.average_filter(image)
        elif mutation_type == 1:
            return filter.median_filter(image)
        elif mutation_type == 2:
            return filter.gaussian_blur(image)
        else:
            return filter.non_local_mean(image)

    def run(self, debug_mode=0, sLogFile=''):
        if not sLogFile:
            if config.NOISE_TYPE == 'gaussian':
                sLogFile = 'ga_under_gaussian_noise'
            else:
                sLogFile = 'ga_under_salt_and_pepper_noise'
        if debug_mode:
            log.log(sLogFile, 'start. image noise {}'.format(debug_mode))
        else:
            log.log(sLogFile, 'start. image noise {}'.format(config.NOISE_SIGMA))
        population = self.init_population()
        fitness_list = self.fitness_list(population)
        fitness_vals = [i[0] for i in fitness_list]
        log.log(sLogFile, 'initial fitness min {} max {}'.format(min(fitness_vals), max(fitness_vals)))

        clean_image = cv.imread('tmp/image.tiff', 1)
        for _ in range(config.GA_ITERATION_MAX):
            pop_new = np.zeros((config.GA_POPULATION_SIZE, *self.image_shape), dtype=np.uint8)
            current_parents = [i[1] for i in fitness_list]
            fitness_val_list = [i[0] for i in fitness_list]
            for i in range(config.GA_POPULATION_SIZE):  # generate offsprings
                parent = self.selection(current_parents, fitness_val_list)
                new_individual = self.crossover(parent)
                new_individual = self.mutation(new_individual)
                pop_new[i] = new_individual.astype('uint8')
            fitness_list.extend(self.fitness_list(pop_new))
            fitness_list.sort(key=lambda x:x[0], reverse=True)
            fitness_list = fitness_list[:config.GA_POPULATION_SIZE]

            current_best = fitness_list[0]
            current_best_image = current_best[1]
            min_fit, max_fit = fitness_list[-1][0], current_best[0]
            log_text = 'fitness min {} max {} SNR:{} PSNR:{} SSIM:{}'.format(min_fit, max_fit, metrics.SNR(clean_image, current_best_image), metrics.PSNR(clean_image, current_best_image), metrics.SSIM(clean_image, current_best_image))
            log.log(sLogFile, log_text)
            print(log_text)

            if (max_fit - min_fit) / max_fit < 0.01:
                break

        max_fitness = fitness_list[0]
        return max_fitness[1].astype('uint8')    # return the best fit denoising image


if __name__ == '__main__':
    if config.BATCH_MODE:
        for i in range(5, 105, 5):
            sigma = i / 100

            noise_image = None
            if config.NOISE_TYPE == 'gaussian':
                noise_image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), 1)
            else:
                noise_image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), 1)
            
            np.random.seed(0)
            obj = GA(noise_image)
            denoised_image = obj.run(debug_mode=sigma)
            cv.imwrite('tmp/denoising_ga_noisesigma_{}.tiff'.format(sigma), denoised_image)
    else:
        noise_image = None
        if config.NOISE_TYPE == 'gaussian':
            noise_image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(config.NOISE_SIGMA), 1)
        else:
            noise_image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(config.NOISE_SIGMA), 1)

        np.random.seed(0)
        obj = GA(noise_image)
        denoised_image = obj.run()
        cv.imwrite('tmp/denoising_ga_noisesigma_{}.tiff'.format(config.NOISE_SIGMA), denoised_image)


