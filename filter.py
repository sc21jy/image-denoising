import bm3d
import scipy
import numpy as np
import skimage
import cv2 as cv
import config

def BM3D(image):
    return np.rint(bm3d.bm3d(image, image.shape)).astype('uint8')

def gaussian_blur(image):
    return cv.GaussianBlur(image, (5, 5), cv.BORDER_DEFAULT).astype('uint8')

def average_filter(image):
    return cv.blur(image, (5, 5)).astype('uint8')

def median_filter(image):
    return cv.medianBlur(image, 5).astype('uint8')

def non_local_mean(image):
    float_image = skimage.img_as_float(image)
    denoised_image = skimage.restoration.denoise_nl_means(float_image, patch_size=5, patch_distance=6, channel_axis=-1)
    return skimage.img_as_ubyte(denoised_image).astype('uint8')

if __name__ == '__main__':
    import metrics
    import log

    sLogFile = ''
    if config.NOISE_TYPE == 'gaussian':
        sLogFile = 'filter_under_gaussian_noise'
    else:
        sLogFile = 'filter_under_salt_and_pepper_noise'

    if config.BATCH_MODE:
        for i in range(5, 105, 5):
            sigma = i / 100

            image = None
            if config.NOISE_TYPE == 'gaussian':
                image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), 1)
            else:
                image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), 1)

            clean_image = cv.imread('tmp/image.tiff', 1)

            log.log(sLogFile, 'noise std:{}'.format(sigma))
            print('noise SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))
            log.log(sLogFile, 'noise image SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))

            for function in (BM3D, gaussian_blur, median_filter, average_filter, non_local_mean):
                denoised_image = function(image)
                print('{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))
                log.log(sLogFile, '{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))
                cv.imwrite('tmp/denoising_{}_noisesigma_{}.tiff'.format(function.__name__, sigma), denoised_image)
    else:
        image = None
        sigma = config.NOISE_SIGMA
        if config.NOISE_TYPE == 'gaussian':
            image = cv.imread('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), 1)
        else:
            image = cv.imread('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), 1)

        clean_image = cv.imread('tmp/image.tiff', 1)

        log.log(sLogFile, 'noise std:{}'.format(sigma))
        print('noise SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))
        log.log(sLogFile, 'noise image SNR:{} PSNR:{} SSIM:{}'.format(metrics.SNR(clean_image, image), metrics.PSNR(clean_image, image), metrics.SSIM(clean_image, image)))

        for function in (BM3D, gaussian_blur, median_filter, average_filter, non_local_mean):
            denoised_image = function(image)
            print('{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))
            log.log(sLogFile, '{} SNR:{} PSNR:{} SSIM:{}'.format(function.__name__, metrics.SNR(clean_image, denoised_image), metrics.PSNR(clean_image, denoised_image), metrics.SSIM(clean_image, denoised_image)))
            cv.imwrite('tmp/denoising_{}_noisesigma_{}.tiff'.format(function.__name__, sigma), denoised_image)
