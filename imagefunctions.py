from PIL import Image
import cv2 as cv
import numpy as np
import config

def generate_noise_image(image_path, debug_mode=0):
    im = Image.open(image_path)
    tiff_image_path = 'tmp/image.tiff'
    im.save(tiff_image_path, 'TIFF')    # convert JPG to tiff
    im = cv.imread(tiff_image_path, 1)  # reread from tiff

    if config.NOISE_TYPE == 'salt and pepper':
        if debug_mode:
            sigma = debug_mode
        else:
            sigma = config.NOISE_SIGMA
        noise_image = im
        mask = np.random.choice((0, 1, 2), size=noise_image.shape, p=[(1-sigma), sigma / 2., sigma / 2.])
        noise_image[mask == 1] = 255
        noise_image[mask == 2] = 0
        cv.imwrite('tmp/noise_salt_and_pepper_sigma_{}.tiff'.format(sigma), noise_image)
    else:
        if debug_mode:
            sigma = debug_mode
        else:
            sigma = config.NOISE_SIGMA
        noise = np.zeros_like(im)
        noise = cv.randn(noise, (0, 0, 0), np.dot(sigma ,(255, 255, 255)))
        noise_image = cv.add(im, noise)
        cv.imwrite('tmp/noise_gaussian_sigma_{}.tiff'.format(sigma), noise_image)

    return noise_image

if __name__ == '__main__':
    if config.BATCH_MODE:
        for i in range(5, 105, 5):
            generate_noise_image(config.INPUT_IMAGE, i/100)
    else:
        generate_noise_image(config.INPUT_IMAGE)

